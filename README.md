# TrabalhoED2Pt2-2020.3ERE

Trabalho Prático - Parte II | DCC012 - Estrutura de Dados II | 2020.3 - ERE - UFJF

 * **André Luiz dos Reis - 201965004AC**

 * **Ester Goulart Cruz Rodrigues - 201965028AC**

 * **Lucca Oliveira Schrôder - 201765205AC**  

 * **Rodrigo da Silva Soares - 201765218AB**

Instruções Iniciais:

Estrutura de arquivos:
1) projeto: contém todos os arquivos .cpp e .h da aplicação;

2) scripts:
    2.1 - compilar.sh: faz apenas a compilação do programa;

    2.2 - executar.sh: inicia o executável

    2.3 - run.sh: junção dos dois scripts acima; (Recomendamos o uso direto desse)

    2.4 - No script run.sh há um caminho para o diretório de arquivos, se necessário, 
          faça a alterações do caminho passar como parametro antes da execução, pois sua passagem é obrigatória.

Método Main:

    * Para fazer uma inserção nova na Tabela Hash e árvores, executação novamente;

    * Restando seguir Instruções da Execução;
  

Caso houver dúvidas relacionadas à navegação ou execução do código, os responsáveis deverão ser consultados!
