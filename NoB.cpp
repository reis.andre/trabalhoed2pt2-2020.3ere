#include "noB.hpp"
#include "Dado.h"

NoB::NoB(int ordem, bool folha)
{
    this->ordem = ordem;
    this->folha = folha;

    this->infos = Dado[2 * ordem - 1];
    this->filhos = new NoB *[2 * ordem];

    this->totalElementos = 0;
}

void NoB::imprime(int tab)
{
    for (int i = 0; i < tab; i++)
        printf("\t");

    int i;
    for (i = 0; i < totalElementos; i++)
    {
        if (!folha)
        {
            tab++;
            filhos[i]->imprime(tab);
        }
        printf("%lld ", infos[i]);
    }
    cout << endl;

    if (!folha)
    {
        filhos[i]->imprime(tab);
    }
}

NoB *NoB::busca(Dado info)
{
    int i = 0;

    while (i < totalElementos && info > infos[i])
        i++;

    if (infos[i] == info)
        return this;

    if (folha == true)
        return NULL;

    return filhos[i]->busca(info);
}

int NoB::procuraIndice(Dado info)
{
    int indice = 0;

    while (indice < totalElementos && infos[indice] < info)
        ++indice;

    return indice;
}

void NoB::remove(Dado info)
{
    int indice = procuraIndice(info);

    if (indice < totalElementos && infos[indice] == info)
    {
        if (folha)
            removeFolha(indice);
        else
            removeNaoFolha(indice);
    }
    else
    {
        if (folha)
        {
            return;
        }

        bool flag = ((indice == totalElementos) ? true : false);

        if (filhos[indice]->totalElementos < ordem)
            preencher(indice);

        if (flag && indice > totalElementos)
            filhos[indice - 1]->remove(info);
        else
            filhos[indice]->remove(info);
    }
    return;
}

void NoB::removeFolha(int indice) // Remove um nó Folha
{
    for (int i = indice + 1; i < totalElementos; ++i)
        infos[i - 1] = infos[i];

    totalElementos--;

    return;
}

void NoB::removeNaoFolha(int indice) // Remove um nó que não folha
{
    int k = infos[indice];

    if (filhos[indice]->totalElementos >= ordem)
    {
        int antecessor = getAntecessor(indice);
        infos[indice] = antecessor;
        filhos[indice]->remove(antecessor);
    }

    else if (filhos[indice + 1]->totalElementos >= ordem)
    {
        int sucessor = getSucessor(indice);
        infos[indice] = sucessor;
        filhos[indice + 1]->remove(sucessor);
    }

    else
    {
        juntar(indice);
        filhos[indice]->remove(k);
    }
    return;
}

int NoB::getAntecessor(int indice) // Retorna o nó anterior ao nó correspondente ao índice
{
    NoB *antecessor = filhos[indice];
    while (!antecessor->folha)
        antecessor = antecessor->filhos[antecessor->totalElementos];

    return antecessor->infos[antecessor->totalElementos - 1];
}

int NoB::getSucessor(int indice) // Retorna o próximo nó do nó correspondente ao índice
{
    NoB *sucessor = filhos[indice + 1];
    while (!sucessor->folha)
        sucessor = sucessor->filhos[0];

    return sucessor->infos[0];
}

void NoB::preencher(int indice)
{
    if (indice != 0 && filhos[indice - 1]->totalElementos >= ordem)
        emprestaAnterior(indice);

    else if (indice != totalElementos && filhos[indice + 1]->totalElementos >= ordem)
        emprestaProximo(indice);

    else
    {
        if (indice != totalElementos)
            juntar(indice);
        else
            juntar(indice - 1);
    }
    return;
}

void NoB::emprestaAnterior(int indice) //seleciona o elemento mais a direita do irmão e posiciona na esquerda do filho
{
    NoB *filho = filhos[indice];
    NoB *irmao = filhos[indice - 1];

    for (int i = filho->totalElementos - 1; i >= 0; --i)
        filho->infos[i + 1] = filho->infos[i];

    if (!filho->folha)
    {
        for (int i = filho->totalElementos; i >= 0; --i)
            filho->filhos[i + 1] = filho->filhos[i];
    }

    filho->infos[0] = infos[indice - 1];

    if (!filho->folha)
        filho->filhos[0] = irmao->filhos[irmao->totalElementos];

    infos[indice - 1] = irmao->infos[irmao->totalElementos - 1];

    filho->totalElementos += 1;
    irmao->totalElementos -= 1;

    return;
}

void NoB::emprestaProximo(int indice) //selecions o elemento mais a esquerda do irmão e posiciona a direita do filho
{
    NoB *filho = filhos[indice];
    NoB *irmao = filhos[indice + 1];

    filho->infos[(filho->totalElementos)] = infos[indice];

    if (!(filho->folha))
        filho->filhos[(filho->totalElementos) + 1] = irmao->filhos[0];

    infos[indice] = irmao->infos[0];

    for (int i = 1; i < irmao->totalElementos; ++i)
        irmao->infos[i - 1] = irmao->infos[i];

    if (!irmao->folha)
    {
        for (int i = 1; i <= irmao->totalElementos; ++i)
            irmao->filhos[i - 1] = irmao->filhos[i];
    }

    filho->totalElementos += 1;
    irmao->totalElementos -= 1;

    return;
}

void NoB::juntar(int indice) // junta dois filhos em um mesmo nó
{
    NoB *filho = filhos[indice];
    NoB *irmao = filhos[indice + 1];

    filho->infos[ordem - 1] = infos[indice];

    for (int i = 0; i < irmao->totalElementos; ++i)
        filho->infos[i + ordem] = irmao->infos[i];

    if (!filho->folha)
    {
        for (int i = 0; i <= irmao->totalElementos; ++i)
            filho->filhos[i + ordem] = irmao->filhos[i];
    }

    for (int i = indice + 1; i < totalElementos; ++i)
        infos[i - 1] = infos[i];

    for (int i = indice + 2; i <= totalElementos; ++i)
        filhos[i - 1] = filhos[i];

    filho->totalElementos += irmao->totalElementos + 1;
    totalElementos--;

    delete (irmao);
    return;
}

void NoB::divideFilhos(int i, NoB *n)
{
    NoB *z = new NoB(n->ordem, n->folha);
    z->totalElementos = ordem - 1;

    for (int j = 0; j < ordem - 1; j++)
        z->infos[j] = n->infos[j + ordem];

    if (n->folha == false)
    {
        for (int j = 0; j < ordem; j++)
            z->filhos[j] = n->filhos[j + ordem];
    }

    n->totalElementos = ordem - 1;
    for (int j = totalElementos; j >= i + 1; j--)
        filhos[j + 1] = filhos[j];

    filhos[i + 1] = z;

    for (int j = totalElementos - 1; j >= i; j--)
        infos[j + 1] = infos[j];

    infos[i] = n->infos[ordem - 1];
    totalElementos = totalElementos + 1;
}

void NoB::insereNaoCheio(long long int info)
{

    int i = totalElementos - 1;

    if (folha == true)
    {
        while (i >= 0 && infos[i] > info)
        {
            infos[i + 1] = infos[i];
            i--;
        }

        infos[i + 1] = info;
        totalElementos = totalElementos + 1;
    }
    else
    {
        while (i >= 0 && infos[i] > info)
            i--;

        if (filhos[i + 1]->totalElementos == 2 * ordem - 1)
        {
            divideFilhos(i + 1, filhos[i + 1]);

            if (infos[i + 1] < info)
                i++;
        }
        filhos[i + 1]->insereNaoCheio(info);
    }
}

void NoB::insereNaoCheio(Dado info)
{

    int i = totalElementos - 1;

    if (folha == true)
    {
        while (i >= 0 && infos[i] > info)
        {
            infos[i + 1] = infos[i];
            i--;
        }

        infos[i + 1] = info;
        totalElementos = totalElementos + 1;
    }
    else
    {
        while (i >= 0 && infos[i] > info)
            i--;

        if (filhos[i + 1]->totalElementos == 2 * ordem - 1)
        {
            divideFilhos(i + 1, filhos[i + 1]);

            if (infos[i + 1] < info)
                i++;
        }
        filhos[i + 1]->insereNaoCheio(info);
    }
}
