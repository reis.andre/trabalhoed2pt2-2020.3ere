/*
 * Arquivo principal do projeto
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004A
 *          Ester Goulart Cruz Rodrigues - 201965028AC
 *          Lucca Oliveira Schrôder - 201765205AC
 *          Rodrigo da Silva Soares - 201765218AB
 * Data de Criação:  10/01/2021
*/
#include <time.h>
#include <chrono>
#include <iomanip>
#include <iostream>

#include "headers/ModuloTeste.h"

using namespace std;

int main(int argc, char *argv[])
{

    //cout << "Iniciando main" << endl; 

    srand(time(NULL)); //seta a semente de tempo para ser aleatorio
    ModuloTeste* t = NULL;

    // Verifica se atende o mínimo de parâmetros
    if (argc != 2)
    {
        cout << "E necessario passar o caminho do diretorio com os arquivo de entrada" << endl;
        exit(1);
    }

    try{

    const char *entrada = argv[1]; 

    cout << "METODO MAIN" << endl;;
    cout << "Diretorio de entrada: " << entrada << endl << endl;

    t = new ModuloTeste(entrada);

    }catch(const std::invalid_argument& e){
       cout << "### ERROR ARGUMENTO INVALIDO ###" << endl;
       std::cerr << e.what() << '\n';

   }
   catch(const std::exception& e){
       cout << "### ERROR ###" << endl;
       std::cerr << e.what() << '\n';
   }

    if(t != NULL) delete t;

    cout << "ENCERRANDO MAIN" << endl;


    return 0;
}