/*
 * Arquivo base da Classe NoQuadTree
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004A
 *          Ester Goulart Cruz Rodrigues - 201965028AC
 *          Lucca Oliveira Schrôder - 201765205AC
 *          Rodrigo da Silva Soares - 201765218AB
 * Data de Criação:  08/03/2021
*/

#ifndef NOQUADTREE_H
#define NOQUADTREE_H

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
using namespace std;

class NoQuadTree {
    private:
    string codEstado, nomeCidade;
    int codCidade;
    float latitude, longitude;
    bool capital;


    //int CoodX, CoodY; //coordenadas do nó

    //filhos da arvore
    NoQuadTree* NW;
    NoQuadTree* NE;
    NoQuadTree* SW;
    NoQuadTree* SE;

    public:
    NoQuadTree(string codEstado, int codCidade, string nomeCidade, float latitude, float longitude, bool capital); 
    ~NoQuadTree();

    string compara(NoQuadTree* P);
    NoQuadTree* quadrante(string sigla);
    void Setquadrante(string sigla, NoQuadTree* novo);
    float getLat();
    float getLog();
    bool equal(NoQuadTree* P);
    void impressao(int prof);
    void arquivoImpressao(ofstream* arqSaida, int prof);// Escreve a impressão em um arquivo
    void pesquisaCodCidade(float latitudeInicial, float longitudeInicial,  float latitudeFinal, float longitudeFinal, vector<int>* vec, int* comp);
    
};

#endif // NOQUADTREE_H