/*
 * Arquivo base da Classe ModuloTeste
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004A
 *          Ester Goulart Cruz Rodrigues - 201965028AC
 *          Lucca Oliveira Schrôder - 201765205AC
 *          Rodrigo da Silva Soares - 201765218AB
 * Data de Criação:  10/03/2021
*/

#ifndef MODULOTESTE_H
#define MODULOTESTE_H


#include <iostream>
#include <string>
#include "ArqLeitura.h"

using namespace std;

class ModuloTeste{
    private:
    ArvoreAVL* avl;
    ArvoreB* arvb;
    QuadTree* quadtree;
    Hash* hash;
    string diretorio;
    ofstream* arqSaida;
    ofstream* arqSaidaModuloTeste;
    ArqLeitura* leitura;
    int tamArvB;

    void insereQuadTree(int N);
    void insereTabelaHash(int N);
    void insereArvoreAVL(int N);
    void insereArvoreB(int N);
    void analiseMetrica();
    //void menuMetrica();

    public:
    ModuloTeste(string diretorio);
    ~ModuloTeste();
    void menu(); //alguma entrada????
    
    
};

#endif // MODULOTESTE_H