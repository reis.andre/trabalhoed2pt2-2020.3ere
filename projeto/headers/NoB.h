/*
 * Arquivo base da Classe NoB
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004A
 *          Ester Goulart Cruz Rodrigues - 201965028AC
 *          Lucca Oliveira Schrôder - 201765205AC
 *          Rodrigo da Silva Soares - 201765218AB
 * Data de Criação:  10/01/2021
*/

#include <iostream>
#include <fstream>

#include "Hash.h"


using namespace std;

class NoB
{
private:
    int *infos; // NAO VAI GUARDAR DADOS, E SIM O COD HASH
    int ordem; // ORDEM EH O TAMANHO MINIMO DE ELEMENTOS ANTE A IMPLEMENTAÇÃO
    NoB **filhos;
    int totalElementos;
    bool folha;

public:
    NoB(int ordem, bool folha);
    ~NoB(); // precisa fazer o destrutor / ponteiros

    void insereAux(int info);
    void divideFilhos(int i, NoB *n);
    void imprime(int tab);
    NoB *busca(int info);
    int getAntecessor(int indice);
    int getSucessor(int indice);
    void setTotalElementos(int totalElementos) { this->totalElementos = totalElementos; };
    int getTotalElementos() { return totalElementos; };
    int getInfo(int indice) { return infos[indice]; };
    void setFilho(int indice, NoB *filho) { this->filhos[indice] = filho; }; 
    NoB *getFilho(int indice) { return filhos[indice]; };
    void auxTotalCidade(int codCidade, int* cont, Hash* hash, int* comp); //aux contar total de casos numa cidade dado um codigo
    void arquivoImpressao(ofstream* arqSaida, int tab);// Escreve a impressão em um arquivo
};