/*
 * Arquivo base da Classe QuadTree
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004A
 *          Ester Goulart Cruz Rodrigues - 201965028AC
 *          Lucca Oliveira Schrôder - 201765205AC
 *          Rodrigo da Silva Soares - 201765218AB
 * Data de Criação:  08/03/2021
*/

#ifndef QUADTREE_H
#define QUADTREE_H

#include <iostream>
#include <string>
#include <vector>

#include "NoQuadTree.h"
using namespace std;


class QuadTree {
    private:

    NoQuadTree* raiz; // dados do nó
    void inserirAux(NoQuadTree* P);
    //CAMPO: contém informações descritivas sobre o nó, por exemplo, nome da cidade.
    //X e Y: Coordenadas do ponto
    //NE,NW,SW,SE: ponteiros para os 4 quadrantes filhos

    
       
    public:
    QuadTree(); 
    ~QuadTree();
    void inserir(string codEstado, int codCidade, string nomeCidade, float latitude, float longitude, bool capital);
    //void insert(Node*); 
    vector<int>* pesquisaCodCidade(float latitudeInicial, float longitudeInicial,  float latitudeFinal, float longitudeFinal, int* comp); 
    //bool inBoundary(Point); 
    void impressao();
    void arquivoImpressao(ofstream* arqSaida);


};

#endif // QUADTREE_H