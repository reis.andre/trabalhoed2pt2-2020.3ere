/*
 * Arquivo base da Classe Hash
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004A
 *          Ester Goulart Cruz Rodrigues - 201965028AC
 *          Lucca Oliveira Schrôder - 201765205AC
 *          Rodrigo da Silva Soares - 201765218AB
 * Data de Criação:  18/02/2021
*/

#include "Dado.h"

#ifndef HASH_H
#define HASH_H

#include <iostream>
#include <string>
#include <fstream>
using namespace std;


class Hash {
    private:

        struct entrada {
            Dado* dado;
            int index = -1; //index da permutacao
            string keyData;
            int keyCod;
            int k;
        };

        //tamanho total da tabela a ser alocada
        static const int MAX_SIZE = 1600000; //2^21
        entrada tabela[MAX_SIZE];

        int tamanho; //tamanho relativo ao numero de posições ocupadas
        int colisoes;
        
        int funcaoHash(string data, int cod, int i);
        int randInt(); 


    public:
        Hash();
        ~Hash();
        int getColisoes();
        bool isFull();
        int inserir(string data, int cod, Dado* dado);
        bool remover(string data, int cod);
        Dado* buscar(string data, int cod);
        Dado* buscarHash(int codHash);
        void imprimir();
        int geraChaveUnica(string data, int cod);
        void arquivoImpressao(ofstream* arqSaida);

    
};

#endif // HASH_H