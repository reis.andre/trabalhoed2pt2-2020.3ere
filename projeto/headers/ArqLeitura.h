/*
 * Arquivo base da Classe ArqLeitura
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004A
 *          Ester Goulart Cruz Rodrigues - 201965028AC
 *          Lucca Oliveira Schrôder - 201765205AC
 *          Rodrigo da Silva Soares - 201765218AB
 * Data de Criação:  08/03/2021
*/

#ifndef ARQLEITURA_H
#define ARQLEITURA_H

#include "Hash.h"
#include "QuadTree.h"
#include "ArvoreAVL.h"
#include "ArvoreB.h"
#include <iostream>
#include <string>
#include <set>

#define tamArqHash 1431491
#define tamArqCoor 5571
#define NomeArqHash "brazil_covid19_cities_processado.csv"
#define NomeArqCoor "brazil_cities_coordinates.csv"

using namespace std;


class ArqLeitura {
    private:   
    string diretorio;
    //int tamRandomizadas;
    
    public:
    ArqLeitura(string diretorio); 
    void leituraQuadTree(QuadTree* quadTree);
    void leituraTabelaHashArvores(Hash* tabelaHash, ArvoreAVL* arvAvl, ArvoreB* arvB);
    void leituraRandomizadaQuadTree(int tamRandomizadas, QuadTree* quadTree);
    void leituraRandomizadaHashArvores(int tamRandomizadas, Hash* tabelaHash, ArvoreAVL* arvAvl, ArvoreB* arvB);

};

#endif // ARQLEITURA_H
