/*
 * Arquivo base da Classe NoArvoreAVL
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004A
 *          Ester Goulart Cruz Rodrigues - 201965028AC
 *          Lucca Oliveira Schrôder - 201765205AC
 *          Rodrigo da Silva Soares - 201765218AB
 * Data de Criação:  10/01/2021
*/

#ifndef NOHARVOREAVL_H
#define NOHARVOREAVL_H

#include <iostream> 
#include <fstream>
#include "Dado.h"
#include "Hash.h"
using namespace std;

// função que vai permitir diminuir o número de métodos de rotação
// acessa as subárvores e facilita para saber qual caminho deve seguir
enum Direcao // enum é uma forma de declarar constantes (podia ser utilizado o #define)
{
    esquerda = 0, // subárvore da esquerda tem maior altura, devemos caminhar para esquerda ou acessar a subárvore da esquerda
    direita = 1, // subárvore da direita tem maior altura, devemos caminhar para direita ou acessar a subárvore da direita
    igual = 2, //// subárvore com alturas iguais
}; 

class nohArvoreAVL
{
    public:
    nohArvoreAVL(int key);
    ~nohArvoreAVL();

    unsigned short balanceamento; // balanceamento do nó
    nohArvoreAVL *subarvore[2]; // filhos da arvore, subarvore[esquerda] ou subarvore[direita], criado para facilitar na hora de rotacionar a árvore
    void auxTotalCidade(int codCidade, int* cont, Hash* hash, int* comp); //contar total de casos numa cidade dado um codigo
    int chave;
    void imprime(int tab);
    void arquivoImpressao(ofstream* arqSaida, int tab);// Escreve a impressão em um arquivo
    int getKey(){ return chave;};
    //void setDir(nohArvoreAVL* aux){subarvore[1] = aux;};
    //void setEsq(nohArvoreAVL* aux){subarvore[0] = aux;};
};

#endif