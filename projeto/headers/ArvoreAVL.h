/*
 * Arquivo base da Classe ArvoreAVL
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004A
 *          Ester Goulart Cruz Rodrigues - 201965028AC
 *          Lucca Oliveira Schrôder - 201765205AC
 *          Rodrigo da Silva Soares - 201765218AB
 * Data de Criação:  10/01/2021
*/

#ifndef ARVOREAVL_H
#define ARVOREAVL_H

#include <fstream>
#include "nohArvoreAVL.h"
#include "Hash.h"
using namespace std;

class ArvoreAVL 
{
    private:
    nohArvoreAVL *raiz; 
    Hash *hash;

    public:
    int chave;
    ArvoreAVL(Hash *hash);
    ~ArvoreAVL();
    void inserir(int chaveIns); 
    void auxInserir(int chaveIns, nohArvoreAVL *no, bool alturaMudou, nohArvoreAVL *pai, Direcao dir); 
    // rebalanceamento:
    void atualizarBalanceamento(nohArvoreAVL *no, Direcao dir);
    void rebalancearInsercao(nohArvoreAVL *no, Direcao dir, bool alturaMudou);
    bool buscar(int chaveBusc);
    // rotações na árvore:
    void rotacaoSimples(nohArvoreAVL *no, Direcao dir);
    void rotacaoDupla(nohArvoreAVL *no, Direcao dir);
    int totalCidade(int codCidade, int* comp);
    inline Direcao oposta(Direcao dir);
    void imprime();
    void arquivoImpressao(ofstream* arqSaida);
    
};

#endif