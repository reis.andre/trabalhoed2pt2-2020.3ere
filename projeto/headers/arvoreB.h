/*
 * Arquivo base da Classe ArvoreB
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004A
 *          Ester Goulart Cruz Rodrigues - 201965028AC
 *          Lucca Oliveira Schrôder - 201765205AC
 *          Rodrigo da Silva Soares - 201765218AB
 * Data de Criação:  10/01/2021
*/

#include <iostream>
#include <fstream>
#include <algorithm>
#include "NoB.h"
//teste
using namespace std;

class ArvoreB
{
private:
    NoB *raiz;
    Hash *hash;
    int ordem;

public:
    ArvoreB(int ordem, Hash *hash);
    ~ArvoreB();
    void imprime();
    void arquivoImpressao(ofstream* arqSaida);// Escreve a impressão em um arquivo
    NoB *busca(int info);
    void insere(int info);
    int totalCidade(int codCidade, int* comp);
};