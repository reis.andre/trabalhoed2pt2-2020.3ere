/*
 * Arquivo base da Classe Dado
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004A
 *          Ester Goulart Cruz Rodrigues - 201965028AC
 *          Lucca Oliveira Schrôder - 201765205AC
 *          Rodrigo da Silva Soares - 201765218AB
 * Data de Criação:  18/02/2021
*/

#ifndef DADO_H
#define DADO_H

#include <iostream>
#include <string>
using namespace std;

class Dado {
    private:
       
    string data;
    string estado;
    string nome_cidade;
    int cod_cidade;
    int num_casos;
    int num_obitos;

    public:
    Dado();
    Dado(string data, string estado, string nome_cidade, int cod_cidade, int num_casos, int num_obitos);
    string getData();
    string getEstado();
    string getCidade();
    int getCodCidade();
    int getCasos();
    int getObitos();
    void setCasos(int x);
    void setObitos(int x);   
    
};

#endif // DADO_H
