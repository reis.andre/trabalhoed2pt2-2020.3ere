/*
 * Arquivo de implementação da classe Hash
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004A
 *          Ester Goulart Cruz Rodrigues - 201965028AC
 *          Lucca Oliveira Schrôder - 201765205AC
 *          Rodrigo da Silva Soares - 201765218AB
 * Data de Criação:  18/02/2021
*/

#include "./headers/Hash.h"
#include "./headers/Dado.h"


#include <iostream>
#include <string>
#include <sstream>
#include <time.h>
using namespace std;

//Construtor
Hash::Hash() {
    this->tamanho = 1;
    this->colisoes = 0;
    srand(time(NULL));
    //cout << "Criando Hash" << endl;
}

Hash::~Hash() {
    //cout << "Destruindo Hash" << endl;
    for (int i = 0; i < MAX_SIZE; i++) {        
        if (this->tabela[i].index != -1)
            delete this->tabela[i].dado;
    }
    //cout << "Hash destruido" << endl;
}

int Hash::getColisoes() {
    return this->colisoes;
}

//retorna um inteiro randomico
int Hash::randInt() {
    return (rand() % MAX_SIZE);
}

//verifica se a tabela está cheia
bool Hash::isFull() {
    if (this->tamanho == MAX_SIZE)
        return true;
    return false;
}


//FUNCAO HASH utilizando a sondagem dupla
int Hash::funcaoHash(string data, int cod, int i) {
    std::stringstream s;
    s << cod;
    string codigo = s.str();

    int a = 0, d = 0;

    int estado = codigo[0] + codigo[1];

    for (int i=2; i < codigo.size(); i++) {
        a += codigo[i];
    }

    for (int i=0; i<data.size(); i++) {
        d += data[i];
    }

    int h = estado % 27 + a + d;

    return (h + i*260) % MAX_SIZE;
}


//Função que insere um dado na tabela e retorna verdadeiro, caso seja possivel -1 erro | Posição do Hash
int Hash::inserir(string data, int cod, Dado* dado) {
    
    //cout << "t1" << endl;
    int h;
    if (this->isFull())
        return -1;

    //cout << "t2" << endl;
    for (int i=0; i < MAX_SIZE; i++) {
        h = this->tamanho;
        //h = funcaoHash(data, cod, i);
        //cout << "t3" << endl;
        if (tabela[h].index == -1 || tabela[h].index == -2) { //posição vazia
            tabela[h].dado = dado;
            tabela[h].index = i;
            tabela[h].keyData = data;
            tabela[h].keyCod = cod;
            (this->tamanho)++;
            return h;
        }else {
            //cout << "t4" << endl;
            (this->colisoes)++;
        }
    }
    cout << h << endl;
    return h;
    
}

//Função que remove um dado da tabela e retorna verdadeiro, caso possivel
bool Hash::remover(string data, int cod) {
    
    int h;
    for (int i=0; i < this->tamanho; i++) {
        h = funcaoHash(data, cod, i);

        //se a posicao nao esta vazia e nao foi removida
        if (tabela[h].index != -1 && tabela[h].index != -2) {
            if (tabela[h].keyCod == cod && tabela[h].keyData == data) {
                tabela[h].index = -2;
                (this->tamanho)--;
                return true;
            }
        }
    }
    return false;
}

//Retorna um dado de acordo com as chaves passadas
Dado* Hash::buscar(string data, int cod) {

    int h;
    for (int i = 0; i < this->tamanho; i++) {
        h = funcaoHash(data, cod, i);
        if (tabela[h].index != -1 && tabela[h].index != -2) {
            if (tabela[h].keyData == data && tabela[h].keyCod == cod) {
                return tabela[h].dado;
            }
        }
    }
    return NULL;
}

//Imprime a tabela
void Hash::imprimir() {

    for (int i = 0; i < MAX_SIZE; i++) {
        
        if (this->tabela[i].index != -1) {
            cout << "T [ " << i << " ] --> (" 
            << this->tabela[i].keyData << ", "<< this->tabela[i].keyCod << ")"<< endl;
        }
        
    }

    cout << "colisoes: " << this->getColisoes() << endl << endl;

}

//Busca um dado na tabela através de um codigo hash
Dado* Hash::buscarHash(int codHash){
    
    if (tabela[codHash].index != -1 && tabela[codHash].index != -2)
        return tabela[codHash].dado;
    
    return NULL;
 }

 void Hash::arquivoImpressao(ofstream* arqSaida){

    if(this->tamanho == 0){
        *arqSaida << "Tabela Hash vazia!" << endl;
    }else{
        *arqSaida << "* * Tabela Hash* *" << endl;

        for (int i = 0; i < MAX_SIZE; i++) {

            if (this->tabela[i].index != -1) {
            *arqSaida << "Posição: " << i
            << " | "<< this->tabela[i].dado->getCidade()
            << ", "<< this->tabela[i].keyData
            << ", "<< this->tabela[i].keyCod 
            << ", "<< this->tabela[i].dado->getCasos() << endl;
            }
        }

        *arqSaida << endl;
    }

}
