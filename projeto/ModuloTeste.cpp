/*
 * Arquivo de implementação da classe ModuloTeste
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004A
 *          Ester Goulart Cruz Rodrigues - 201965028AC
 *          Lucca Oliveira Schrôder - 201765205AC
 *          Rodrigo da Silva Soares - 201765218AB
 * Data de Criação:  10/03/2021
*/

#include "./headers/ModuloTeste.h"



#include <iostream>
#include <string>
#include <sstream>
#include <chrono>
#include <iomanip>
#include <time.h>
using namespace std;

ModuloTeste::ModuloTeste(string diretorio) {
    
    try{

    this->diretorio = diretorio;
    avl = NULL; arvb = NULL; quadtree = NULL; hash = NULL;
    
    cout << "* * * INICIANDO MODULO DE TESTE * * *" << endl;

    arqSaida = new ofstream("Impressoes.txt");
    arqSaidaModuloTeste = new ofstream("saida.txt");
    leitura = new ArqLeitura(diretorio);
    this->tamArvB = 3;
    int op, qnt, N;


    this->menu();
    }catch(const std::exception& e){
       cout << "### ERROR ###" << endl;
       std::cerr << e.what() << '\n';
    }

    cout << "* * * ENCERRANDO MODULO DE TESTE * * *" << endl;

}

ModuloTeste::~ModuloTeste(){
    if(arqSaida != NULL) delete arqSaida;
    if(arqSaidaModuloTeste != NULL) delete arqSaidaModuloTeste;
    if(quadtree != NULL) delete quadtree;
    if(arvb != NULL) delete arvb;
    if(avl != NULL) delete avl;
    if(hash != NULL) delete hash;
    if(leitura != NULL) delete leitura;
}


void ModuloTeste::menu() {
    int op, N;

    cout << "1- Insercao de N cidades na QuadTree\n"
         << "2- Insercao de N registros na Tabela Hash, Arvore AVL e B e Impressao Hash\n"
         << "3- Impressao de N chaves na Arvore AVL\n"
         << "4- Impressao de N chaves na Arvore B\n"
         << "5- Geracao de Analise de Metrica de Desempenho\n"
         << "6- Sair" << endl;
    cout << "Escolha uma opcao: ";
    cin >> op;
    if(op ==1 || op == 2){
        cout << "Valor de N: ";
        cin >> N;
    }

    while(op != 6){ // fica no menu até digitar 5
        switch (op)
        {
        case 1:
            insereQuadTree(N);
            break;
        case 2:
            insereTabelaHash(N);
            break;
        case 3:
            insereArvoreAVL(N);
            break;
        case 4:
            insereArvoreB(N);
            break;
        case 5:
            analiseMetrica();
            break;
        default:
            cout << "Opcao invalida!" <<endl;
            break;
        }
        cout<< "1- Insercao de N cidades na QuadTree\n"
          << "2- Insercao de N registros na Tabela Hash, Arvore AVL e B e Impressao Hash\n"
          << "3- Impressao de N chaves na Arvore AVL\n"
          << "4- Impressao de N chaves na Arvore B\n"
          << "5- Geracao de Analise de Metrica de Desempenho\n"
          << "6- Sair" << endl;
        cout << "Escolha uma opcao: ";
        cin >> op;
        if(op ==1 || op == 2){
            cout << "Valor de N: ";
            cin >> N;
        }
    }
}

void ModuloTeste::insereQuadTree(int N) {
    if(this->quadtree == NULL){
        quadtree = new QuadTree();
    }else{
        delete this->quadtree;
        quadtree = new QuadTree();
    }

    leitura->leituraRandomizadaQuadTree(N,quadtree);
    N <= 20 ? quadtree->impressao() : quadtree->arquivoImpressao(arqSaida);
}

void ModuloTeste::insereTabelaHash(int N) {
    //cout << "iniciando insere tabela hash" << endl;
    if(this->hash == NULL){
        hash = new Hash();
    }else{
        delete this->hash;
        hash = new Hash();
        //cout << "Hash: " << hash;
    }

    if(this->avl == NULL){
        avl = new ArvoreAVL(hash);
    }else{
        delete this->avl;
        avl = new ArvoreAVL(hash);
    }

    if(this->arvb == NULL){
        arvb = new ArvoreB(tamArvB,hash);
    }else{
        delete this->arvb;
        arvb = new ArvoreB(tamArvB,hash);
    }
    

    //cout << "iniciando leitura randomizada" << endl;
    this->leitura->leituraRandomizadaHashArvores(N,hash,avl,arvb);
    //cout << "finalizando leitura randomizada" << endl;

    N <= 20 ? this->hash->imprimir() : this->hash->arquivoImpressao(arqSaida);

    //cout << "finalizando insere tabela hash" << endl;
}

void ModuloTeste::insereArvoreAVL(int N) {
    if(this->avl == NULL){
        cout << "Faça a insercao no Hash primeiramente!" << endl;
    }else{
        N <= 20 ? avl->imprime() :avl->arquivoImpressao(arqSaida);
    }
    
}

void ModuloTeste::insereArvoreB(int N) {
    if(this->arvb == NULL){
         cout << "Faça a insercao no Hash primeiramente!" << endl;
    }else{
        N <= 20 ? arvb->imprime() : arvb->arquivoImpressao(arqSaida);
    }
    
}

void ModuloTeste::analiseMetrica(){
    if(this->quadtree == NULL){
        quadtree = new QuadTree();
    }else{
        delete this->quadtree;
        quadtree = new QuadTree();
    }

    leitura->leituraQuadTree(quadtree); // leitura e inserção de todo o arquivo de coordenadas

    //permita ao usuario fornecer os parametros
    int codcidade;
    float xi,xf,yi,yf;
    cout << "Forneca as entradas necessarias separadas por espaco, respectivamente: "
     << endl << "Cod de Cidade para Buscar Total de Casos, Intervalo [(x0, y0), (x1, y1)], onde x0 e x1 sao latitudes e y0 e y1 sao longitudes" << endl;
    cin >> codcidade >> xi >> xf >> yi >> yf; // FUNCIONANDO
    
    //this->leitura->leituraTabelaHashArvores(hash,avl,arvb);
    int vec[] = {10000,50000,100000,500000,1000000}; // tamanhos de instancias desejados
    int tam = 5; // define o tamanho do vetor, se quiser alterar a qnt dos valores de n
    int m = 10; // m = quantidade de repetição para geração da médias da métrica de cada algoritmo
    // ADOTADO M = 2 PARA TESTES RAPIDOS

    chrono::duration<double> tempo;
    int totalqntComp = 0;// contador das métricas para média
    chrono::duration<double> totaltempo = tempo.zero();
    auto start = chrono::steady_clock::now();
    auto end = chrono::steady_clock::now();
    

    *arqSaidaModuloTeste << "*** Relatório para comparações estatísticas *** " << endl << endl;
    int aux = 0;

    for(int i=0; i < tam; i++){ // para cada um dos valores de n, vamos testar o(s) algoritmos
        
        /**** ARVORE ARB1****/  tamArvB = 200;
            *arqSaidaModuloTeste << "*** Tamanho Amostra: " << vec[i] << endl;
            *arqSaidaModuloTeste << "** Busca Cidade Específica ArvB - Ordem: " << tamArvB << endl;
            
            for(int j=0; j < m; j++){ // garante a repetição de cada algoritmo m vezes
                    //cout << "entrei" << endl;
                    this->insereTabelaHash(vec[i]);//faz a leitura do hash e arvores AVL E B   
                    //cout << "sai" << endl;
                    start = chrono::steady_clock::now(); // inicia contagem tempo
                    int total = arvb->totalCidade(codcidade,&aux); //chama o algoritmo desejado
                    auto end = chrono::steady_clock::now(); // termina contagem tempo
                    tempo = end - start; // calcula duração do algoritmo
                    totalqntComp+=aux; //salva total de comp
                    totaltempo+=tempo;// computar somatorio
                    *arqSaidaModuloTeste << fixed << setprecision(2) << "  Iteracao " << j << ": Qnt Comp: " << aux <<
                    " Tempo: " << setprecision(6) << tempo.count() << " Total Casos: " << total << endl;
                    aux=0;
            }

            // calcular média das métricas e gravar no arquivo de saida
            *arqSaidaModuloTeste << fixed << setprecision(2) << "  #Resumo|Medias: " << "Comp: " << totalqntComp/m
            << " Media Tempo: " << setprecision(6) << totaltempo.count()/m << endl << endl;
            totalqntComp=0; // zera os parametros de comparação
            totaltempo = totaltempo.zero(); //= std::chrono::milliseconds::zero();

        /**** ARVORE ARB2****/ tamArvB = 20;
            //*arqSaidaModuloTeste << "*** Tamanho Amostra: " << vec[i] << endl;
            *arqSaidaModuloTeste << "** Busca Cidade Específica ArvB - Ordem: " << tamArvB << endl;
            for(int j=0; j < m; j++){ // garante a repetição de cada algoritmo m vezes
                    this->insereTabelaHash(vec[i]);//faz a leitura do hash e arvores AVL E B   
                    start = chrono::steady_clock::now(); // inicia contagem tempo
                    int total = arvb->totalCidade(codcidade,&aux); //chama o algoritmo desejado
                    auto end = chrono::steady_clock::now(); // termina contagem tempo
                    tempo = end - start; // calcula duração do algoritmo
                    totalqntComp+=aux; //salva total de comp
                    totaltempo+=tempo;// computar somatorio
                    *arqSaidaModuloTeste << fixed << setprecision(2) << "  Iteracao " << j << ": Qnt Comp: " << aux <<
                    " Tempo: " << setprecision(6) << tempo.count() << " Total Casos: " << total << endl;
                    aux=0;
            }

            // calcular média das métricas e gravar no arquivo function escreveArquivoSaidaComparacao(str)
            *arqSaidaModuloTeste << fixed << setprecision(2) << "  #Resumo|Medias: " << "Comp: " << totalqntComp/m
            << " Media Tempo: " << setprecision(6) << totaltempo.count()/m << endl << endl;
            totalqntComp=0; // zera os parametros de comparação
            totaltempo = totaltempo.zero(); //= std::chrono::milliseconds::zero();

        /**** ARVORE AVL****/
            //*arqSaidaModuloTeste << "*** Tamanho Amostra: " << vec[i] << endl;
            *arqSaidaModuloTeste << "** Busca Cidade Especifica AVL " << endl;
            for(int j=0; j < m; j++){ // garante a repetição de cada algoritmo m vezes
                    this->insereTabelaHash(vec[i]);//faz a leitura do hash e arvores AVL E B   
                    start = chrono::steady_clock::now(); // inicia contagem tempo
                    int total = avl->totalCidade(codcidade,&aux); //chama o algoritmo desejado
                    auto end = chrono::steady_clock::now(); // termina contagem tempo
                    tempo = end - start; // calcula duração do algoritmo
                    totalqntComp+=aux; //salva total de comp
                    totaltempo+=tempo;// computar somatorio
                    *arqSaidaModuloTeste << fixed << setprecision(2) << "  Iteracao " << j << ": Qnt Comp: " << aux <<
                    " Tempo: " << setprecision(6) << tempo.count() << " Total Casos: " << total << endl;
                    aux=0;
            }

            // calcular média das métricas e gravar no arquivo function escreveArquivoSaidaComparacao(str)
            *arqSaidaModuloTeste << fixed << setprecision(2) << "  #Resumo|Medias: " << "Comp: " << totalqntComp/m
            << " Media Tempo: " << setprecision(6) << totaltempo.count()/m << endl << endl;
            totalqntComp=0; // zera os parametros de comparação
            totaltempo = totaltempo.zero(); //= std::chrono::milliseconds::zero();

        /**** ARVORE GEO****/
        *arqSaidaModuloTeste << "** Busca Cidade EM REGIÃO via AVL" << endl;
        vector<int>* cidades = quadtree->pesquisaCodCidade(xi,xf,yi,yf, &aux);
        start = chrono::steady_clock::now(); // inicia contagem tempo
        int total = 0;
        vector<int>::iterator v = cidades->begin();
        while( v != cidades->end()){
            cout << "cidade cod: " << *v;
            total+= arvb->totalCidade(*v,&aux);
            v++;
        }
        auto end = chrono::steady_clock::now(); // termina contagem tempo
        tempo = end - start; // calcula duração do algoritmo
         *arqSaidaModuloTeste << fixed << setprecision(2) <<  "Qnt Comp Total: " << aux <<
            " Tempo: " << setprecision(6) << tempo.count() << " Total Casos na região: " << total << endl;
        aux = 0;

        delete cidades;

        *arqSaidaModuloTeste << endl;
    
    }
}
