/*
 * Arquivo de implementação da classe ArvoreB
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004A
 *          Ester Goulart Cruz Rodrigues - 201965028AC
 *          Lucca Oliveira Schrôder - 201765205AC
 *          Rodrigo da Silva Soares - 201765218AB
 * Data de Criação:  08/03/2021
*/

#include "headers/arvoreB.h"

ArvoreB::ArvoreB(int ordem, Hash *hash){
    this->raiz = NULL;
    this->ordem = ordem; // SE FOR O NUMERO MÍNIMO DE ELEMENTOS DE UM NÓ, É PRECISO FAZER A CONVERSAO
    this->hash = hash;
}

ArvoreB::~ArvoreB(){
 //   delete this->raiz; // SÓ CHAMAR APÓS CONSTRUIR DESTRUTOR DO NO
}

void ArvoreB::imprime(){
    if(this->raiz != NULL){
        this->raiz->imprime(0);
    }else{
        cout << "ArvoreB vazia!" << endl;
    }
}

NoB* ArvoreB::busca(int info){ // IMPLEMENTAR AINDA
    return NULL;
}

void ArvoreB::insere(int info){ //EM TESE, SÓ INSERIR, O NO VAI CUIDAR DE TUDO
    if(this->raiz == NULL){ // criar raiz
        this->raiz = new NoB(ordem, true);
        //FAZER A INSERÇÃO DO VALOR AQUI
        raiz->insereAux(info);
    }else{ // chamar a inserção
        //cout << "Total Elementos: "  << raiz->getTotalElementos() << endl;
        //cout << "Total ORDEM: "  << 2*ordem-1 << endl;
        if(raiz->getTotalElementos() == 2*ordem-1){
            //cout << "entrou if" << endl;
            NoB* novaraiz = new NoB(ordem, false);
            novaraiz->setFilho(0,this->raiz);
            novaraiz->divideFilhos(0,this->raiz);
            int i = 0;
            if(novaraiz->getInfo(0) < info){ // TROCAR ISSO PRA COMPARAÇÃO COM DADO, APENAS PRA TESTE
                i++; 
            }
            novaraiz->getFilho(i)->insereAux(info);
            this->raiz = novaraiz;
        }else{
            raiz->insereAux(info);
        }
    }

}

int ArvoreB::totalCidade(int codCidade,int* comp)
{
    int cont = 0;
    if(raiz != NULL)
        raiz->auxTotalCidade(codCidade, &cont, this->hash, comp);
    return cont;
}

void ArvoreB::arquivoImpressao(ofstream* arqSaida){
    if(this->raiz != NULL){
        *arqSaida << "* * ArvoreB * *" << endl;
        this->raiz->arquivoImpressao(arqSaida, 0);
        *arqSaida << endl;
    }else{
        *arqSaida << "ArvoreB vazia!" << endl;
    }
}
