/*
 * Arquivo de implementação da classe ArqLeitura
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004A
 *          Ester Goulart Cruz Rodrigues - 201965028AC
 *          Lucca Oliveira Schrôder - 201765205AC
 *          Rodrigo da Silva Soares - 201765218AB
 * Data de Criação:  08/03/2021
*/

#include "./headers/ArqLeitura.h"
#include <fstream>
#include <regex>
#include <chrono>

ArqLeitura::ArqLeitura(string diretorio){
    this->diretorio = diretorio;
    srand(time(NULL));
    //this->tamRandomizadas = 0;
    //cout << "Criando dado genético" << endl;
}

void ArqLeitura::leituraQuadTree(QuadTree* quadTree){
    auto start = std::chrono::steady_clock::now();
    ifstream arq(diretorio + NomeArqCoor);

    if (arq.is_open())
    {

        cout << "  Abrindo Arquivo: " << diretorio + NomeArqCoor << endl;

        string str, result, teste[6];
        int count = 0, posicao;

        getline(arq, str); //ignora primeira linha

        while (getline(arq, str))
        {

            posicao = 0;
            std::stringstream ssin(str);

            // quebrando a linha linha por , e adicionando ao vetor
            while (std::getline(ssin, result, ','))
            {
                teste[posicao] = result; // state_code,city_code,city_name,lat,long,capital
                posicao++; // considerar somente os 6 primeios dígitos city_code na quadTree
            }

            //adicionar dado a QuadTree
            // criando o dado, adicionando ao vetor e count++
            //Dado *aux = new Dado(teste[0], teste[1], teste[2], std::stoi(teste[3]), std::stoi(teste[4]), std::stoi(teste[5]));
            //instancias[count] = aux;
            quadTree->inserir(teste[0], stoi(teste[1].substr(0,5)), teste[2], std::stof(teste[3]), std::stof(teste[4]), teste[5] == "FALSE" ? false : true);
            count++;
        }
        cout << "  Fim da Leitura | Quantidade de instancias: " << count << endl;
    }
    else
        cerr << "ERRO: O arquivo " << NomeArqCoor << " nao pode ser aberto!" << endl;

    auto end = std::chrono::steady_clock::now();
    std::chrono::duration<double> tempo = end - start;
    std::cout << "  *** Tempo de Processamento da leitura e criacao de dados = " << tempo.count() << "s" << endl << endl;


}

void ArqLeitura::leituraTabelaHashArvores(Hash* tabelaHash, ArvoreAVL* arvAvl, ArvoreB* arvB){
    auto start = std::chrono::steady_clock::now();
    ifstream arq(diretorio + NomeArqHash);

    if (arq.is_open())
    {

        cout << "Abrindo Arquivo: " << diretorio + NomeArqHash << endl;

        string str, result, teste[6];
        int count = 0, posicao, codHash;
        getline(arq, str); //ignora primeira linha

        while (getline(arq, str))
        {

            posicao = 0;
            std::stringstream ssin(str);

            // quebrando a linha linha por , e adicionando ao vetor
            while (std::getline(ssin, result, ','))
            {
                teste[posicao] = result; // state_code,city_code,city_name,lat,long,capital
                posicao++; // considerar somente os 6 primeios dígitos city_code na quadTree
            }

            //adicionar dado a QuadTree
            // criando o dado, adicionando ao vetor e count++
            Dado *aux = new Dado(teste[0], teste[1], teste[2], std::stoi(teste[3]), std::stoi(teste[4]), std::stoi(teste[5]));
            //instancias[count] = aux;

            codHash = tabelaHash->inserir(aux->getData(), aux->getCodCidade(), aux);
            arvAvl->inserir(codHash);
            arvB->insere(codHash);
            count++;
        }
        cout << "Fim da Leitura" << endl
             << "Quantidade de instancias: " << count << endl;
        tabelaHash->imprimir();
    }
    else
        cerr << "ERRO: O arquivo " << NomeArqHash << " nao pode ser aberto!" << endl;

    auto end = std::chrono::steady_clock::now();
    std::chrono::duration<double> tempo = end - start;
    std::cout << "*** Tempo de Processamento da leitura e criacao de dados = " << tempo.count() << "s" << std::endl;


}

void ArqLeitura::leituraRandomizadaQuadTree(int tamRandomizadas, QuadTree* quadTree)
{

    ifstream arq(diretorio + NomeArqCoor);
    //cout << "t**" << endl;

    if (arq.is_open())
    {
        //cout << "t***" << endl;
        //cout << "Abrindo Arquivo Processado" << endl;     
        srand(time(NULL));
        set<int> listaLinhas;        
        string str;
        string result;
        string teste[6];
        int count = 0;
        int posicao = 0; 
        int posicaoUsada = 0;   

        //this->instanciasRandomizadas = new Dado *[N];
        //this->tamRandomizadas = N;

        //cout << "t1" << endl;
        for(int i=0; i<tamRandomizadas; i++){
            //cout << i;
            int a = rand() % tamArqCoor;
            int num = (28741*(rand()%31)) + (3587*(rand()%19)) + (43871*(rand()%7)) - (43587*(rand()%5)) + (92825*(rand()%3)) + (32587*(rand()%2))+ a;
            //funçãoo pra gerar numero da linha aleatorio devido ao RAND_MAX no pc


            if(num >  tamArqCoor) // verifica se o numero não é maior q a qnt de linhas do arquivo e corrigi
                num %=  tamArqCoor;
            
            if(num <1  || listaLinhas.count(num)){ //sem numero <=0 ou já selecionado
                i--;
            }else{         
                listaLinhas.insert(num); // insere no set
            }
        }

        if(listaLinhas.size() != tamRandomizadas){
            cout << "DEU RUIM NA QNT DA LISTA DE RANDOMIZADAS" << endl;
        }


        //cout << "t2" << endl;
        getline(arq, str); //ignora primeira linha

        while (posicaoUsada < tamRandomizadas && getline(arq, str)) // lê até ter o tanto de instancias desejados
        {

            if(listaLinhas.count(count) == 1){ // só considera a linha se for uma linha selecionada

                
                //cout << count << " ";
                posicao = 0;
                std::stringstream ssin(str);

                // quebrando a linha linha por , e adicionando ao vetor
                while (std::getline(ssin, result, ','))
                {
                    teste[posicao] = result;
                    posicao++;
                }

                //PENDENTE CRIAR DADO E INSERIR NA QUADTREE
                // criando o dado, adicionando ao vetor e count++
                //arq << "state,name,date,code,cases,deaths" << endl;
                //Dado *aux = new Dado(teste[2], teste[0], teste[1], std::stoi(teste[3]), std::stoi(teste[4]), std::stoi(teste[5]));
                //instanciasRandomizadas[posicaoUsada] = aux;
                quadTree->inserir(teste[0], stoi(teste[1].substr(0,5)), teste[2], std::stof(teste[3]), std::stof(teste[4]), teste[5] == "FALSE" ? false : true);
                posicaoUsada++;
                //cout << count << endl;
                //cout << posicaoUsada << " ";
            }
            count++;
        }
        cout << "Carregamento dos registros realizada" << endl;
    }
    else
        cerr << "ERRO: O arquivo nao pode ser aberto!" << endl;
}

void ArqLeitura::leituraRandomizadaHashArvores(int tamRandomizadas, Hash* tabelaHash, ArvoreAVL* arvAvl, ArvoreB* arvB){
    ifstream arq(diretorio + NomeArqHash);
    //cout << "t**" << endl;

    if (arq.is_open())
    {
        //cout << "t***" << endl;
        //cout << "Abrindo Arquivo Processado" << endl;     
        srand(time(NULL));
        set<int> listaLinhas;        
        string str;
        string result;
        string teste[6];
        int count = 0;
        int posicao = 0, codHash; 
        int posicaoUsada = 0;   

        //this->instanciasRandomizadas = new Dado *[N];
        //this->tamRandomizadas = N;

        for(int i=0; i<tamRandomizadas; i++){
            //cout << i;
            int a = rand() % tamArqHash;
            int num = (28741*(rand()%31)) + (3587*(rand()%19)) + (43871*(rand()%7)) - (43587*(rand()%5)) + (92825*(rand()%3)) + (32587*(rand()%2))+ a;
            //funçãoo pra gerar numero da linha aleatorio devido ao RAND_MAX no pc


            if(num >=  tamArqHash) // verifica se o numero não é maior q a qnt de linhas do arquivo e corrigi
                num %=  tamArqHash;
            
            if(num < 2  || listaLinhas.count(num) == 1){ //sem numero <=0 ou já selecionado
                i--;
            }else{         
                listaLinhas.insert(num); // insere no set
            }
        }

        if(listaLinhas.size() != tamRandomizadas){
            cout << "DEU RUIM NA QNT DA LISTA DE RANDOMIZADAS: " << listaLinhas.size() << " | " << tamRandomizadas << endl;
        }

        getline(arq, str); //ignora primeira linha

        while (posicaoUsada < tamRandomizadas && getline(arq, str)) // lê até ter o tanto de instancias desejados
        {

            if(listaLinhas.count(count) == 1){ // só considera a linha se for uma linha selecionada

                
                //cout << count << " ";
                posicao = 0;
                std::stringstream ssin(str);

                // quebrando a linha linha por , e adicionando ao vetor
                while (std::getline(ssin, result, ','))
                {
                    teste[posicao] = result;
                    posicao++;
                }

                // criando o dado, adicionando ao vetor e count++
                //arq << "state,name,date,code,cases,deaths" << endl;
                Dado *aux = new Dado(teste[2], teste[0], teste[1], std::stoi(teste[3]), std::stoi(teste[4]), std::stoi(teste[5]));
                //instanciasRandomizadas[posicaoUsada] = aux;
                posicaoUsada++;
                //cout << "tentativa de insercao ";
                codHash = tabelaHash->inserir(aux->getData(), aux->getCodCidade(), aux);
                //cout << "Cod: " << codHash << " | " << teste[2] << " | " << teste[0] << " | " << teste[1] << endl;
                //cout << codHash << endl;
                //auto t = tabelaHash->buscarHash(codHash);
                //t->getData(); t->getCidade();
                arvB->insere(codHash); //**//
                arvAvl->inserir(codHash); //**//
                //count++; // DUPLICADO - ERRO
                //cout << codHash << endl;
            }
            count++;
        }

        //cout << posicaoUsada << " " << tamRandomizadas << " " << listaLinhas.size() << endl;
        //cout << "erro";
        //if(listaLinhas.size() != posicaoUsada){
        //    cout << "DEU RUIM NA QNT DA LISTA DE RANDOMIZADAS: " << listaLinhas.size() << " | " << posicaoUsada << endl;
        //}
        //cout << "Carregamento dos registros realizada" << endl;
    }
    else
        cerr << "ERRO: O arquivo nao pode ser aberto!" << endl;
}
