/*
 * Arquivo de implementação da classe Dado
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004A
 *          Ester Goulart Cruz Rodrigues - 201965028AC
 *          Lucca Oliveira Schrôder - 201765205AC
 *          Rodrigo da Silva Soares - 201765218AB
 * Data de Criação:  18/02/2021
*/

#include "./headers/Dado.h"

Dado::Dado(string data, string estado, string nome_cidade, int cod_cidade, int num_casos, int num_obitos){
    this->data = data; 
    this->estado = estado;
    this->nome_cidade = nome_cidade;
    this->cod_cidade = cod_cidade;
    this->num_casos = num_casos;
    this->num_obitos = num_obitos;
}

Dado::Dado(){
    cout << "Criando dado genético" << endl;
}

string Dado::getData(){
    return this->data;
}

string Dado::getEstado(){
    return this->estado;
}

string Dado::getCidade(){
    return this->nome_cidade;
}

int Dado::getCodCidade(){
    return this->cod_cidade;
}

int Dado::getCasos(){
    return this->num_casos;
}

int Dado::getObitos(){
    return this->num_obitos;
}

void Dado::setCasos(int x){
    this->num_casos = x;
}

void Dado::setObitos(int x){
    this->num_obitos = x;
}