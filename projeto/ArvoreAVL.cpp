/*
 * Arquivo de implementação da classe ArvoreAVL
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004A
 *          Ester Goulart Cruz Rodrigues - 201965028AC
 *          Lucca Oliveira Schrôder - 201765205AC
 *          Rodrigo da Silva Soares - 201765218AB
 * Data de Criação:  08/03/2021
*/

#include "headers/ArvoreAVL.h"
#include <iostream>

ArvoreAVL::ArvoreAVL(Hash *hash) 
{
    this->hash = hash;
    this->raiz = NULL;
}

ArvoreAVL::~ArvoreAVL() 
{
    if(this->raiz != NULL)
    {
        delete raiz;
        this->raiz = NULL;
    }
}

void ArvoreAVL::inserir(int chaveIns) // insere nova entrada na árvore
{
    Direcao dir;
    if(raiz == NULL){
        this->raiz = new nohArvoreAVL(chaveIns);
    }else{
        bool alturaMudou = false;
        this->auxInserir(chaveIns, raiz, alturaMudou, NULL, dir); 
    }
    //cout << "Teste raiz " << raiz << " key: " << raiz->getKey() << endl;
}

void ArvoreAVL::auxInserir(int chaveIns, nohArvoreAVL *no, bool alturaMudou, nohArvoreAVL *pai, Direcao dir) 
{
    //if(chaveIns > 1431494 || chaveIns > 65000) cout << "erro 4" << endl;

    if(no == NULL) // se o no atual é nulo
    {
        //if(pai->subarvore[dir] != NULL) cout << "erro 3" << endl;
        //cout << "Entrou em no == NULL" << endl;
        no = new nohArvoreAVL(chaveIns);
        alturaMudou = true;
        pai->subarvore[dir] = no;
    }
    else if(no->chave == chaveIns) // ja esta na arvore
    {
        cout << "Chave jah existente na AVL" << endl;
        return;
    }
    else // não chegamos ainda no lugar desejado
    {
        //cout << "else" << endl;
        
        Direcao dir;
        Dado *dadoChaveInserida = this->hash->buscarHash(chaveIns);
        Dado *dadoNoh = this->hash->buscarHash(no->chave);

        //if(dadoChaveInserida == NULL) cout << "erro 1" << endl;
        //if(dadoNoh == NULL) cout << "erro 2" << endl;

        if(dadoChaveInserida->getCodCidade() > dadoNoh->getCodCidade()) // verificando cidades
        {
            dir = direita;
        }
        else if(dadoChaveInserida->getCodCidade() < dadoNoh->getCodCidade())
        {
            dir = esquerda;
        }
        else
        {
            if(dadoChaveInserida->getData() > dadoNoh->getData()) // verificando datas
            {
                dir = direita;
            }
            else if(dadoChaveInserida->getData() < dadoNoh->getData())
            {
                dir = esquerda;
            }
        }
        
        alturaMudou = false;
        auxInserir(chaveIns, no->subarvore[dir], alturaMudou, no, dir);
        if(alturaMudou) // se a altura mudou, fazemos o rebalanceamento
        {
            cout << "rebalancear" << endl;
            this->rebalancearInsercao(no, dir, alturaMudou);
        }
    }
}

// Após uma rotação, atualiza os fatores de balanceamento
void ArvoreAVL::atualizarBalanceamento(nohArvoreAVL *no, Direcao dir)
{
    int oposta = this->oposta(dir);
    int bal = no->subarvore[dir]->subarvore[oposta]->balanceamento;

    if(bal == dir) // se o fator está do mesmo lado
    {
        no->balanceamento = igual;
        no->subarvore[dir]->balanceamento = oposta;
    }
    else if(bal == oposta) // se o fator está do lado oposto
    {
        no->balanceamento = dir;
        no->subarvore[dir]->balanceamento = igual;
    }
    else // fator de balanceamento está igual
    {
        no->balanceamento = no->subarvore[dir]->balanceamento = igual;
    }
    no->subarvore[dir]->subarvore[oposta]->balanceamento = igual;
}

// Após uma inserção, ele realiza o rebalanceamento
void ArvoreAVL::rebalancearInsercao(nohArvoreAVL *no, Direcao dir, bool alturaMudou)
{
    int oposta = this->oposta(dir);
    if(no->balanceamento == dir)
    {
        if(no->subarvore[dir]->balanceamento == dir)
        {
            no->subarvore[dir]->balanceamento = 2;
            no->balanceamento = igual;
            rotacaoSimples(no, dir);
        }
        else
        {
            atualizarBalanceamento(no, dir);
            rotacaoDupla(no, dir);
        }
        alturaMudou = false;
    }
    else if(no->balanceamento == oposta)
    {
        no->balanceamento = 2;
        alturaMudou = false;
    }
    else 
    {
        no->balanceamento = dir;
    }
}

bool ArvoreAVL::buscar(int chaveBusc) // procura a chave na árvore
{
    nohArvoreAVL *nohAtual = this->raiz;
    Dado *dadoChaveBuscada = this->hash->buscarHash(chaveBusc);
    Dado *dadoNoh = this->hash->buscarHash(nohAtual->chave);

    while(nohAtual != NULL)
    {

        if(dadoChaveBuscada->getCodCidade() > dadoNoh->getCodCidade()) // chave mais a direita
        {
            nohAtual = nohAtual->subarvore[direita]; 
        }
        else if(dadoChaveBuscada->getCodCidade() < dadoNoh->getCodCidade()) // chave mais a esquerda
        {
            nohAtual = nohAtual->subarvore[esquerda]; 
        }
        else
        {
            if(dadoChaveBuscada->getData() > dadoNoh->getData()) 
            {
                nohAtual = nohAtual->subarvore[direita]; // chave mais a direita
            }
            else if(dadoChaveBuscada->getData() < dadoNoh->getData())
            {
                nohAtual = nohAtual->subarvore[esquerda]; // chave mais a esquerda
            }
            else // chave encontrada
            {
                return true;
            }
        }
        Dado *dadoNoh = this->hash->buscarHash(nohAtual->chave);
    }
    return false; // a chave não está na árvore

}

// Direcao dir é utilizado para definir a direção (esq ou dir) para efetuar a rotação
void ArvoreAVL::rotacaoSimples(nohArvoreAVL *no, Direcao dir) 
{
    int oposta = this->oposta(dir);
    nohArvoreAVL *filho = no->subarvore[dir];
    no->subarvore[dir] = filho->subarvore[oposta];
    filho->subarvore[oposta] = no;
    no = filho;
}

void ArvoreAVL::rotacaoDupla(nohArvoreAVL *no, Direcao dir) 
{
    int oposta = this->oposta(dir);
    nohArvoreAVL *filho = no->subarvore[dir]->subarvore[oposta];
    no->subarvore[dir]->subarvore[oposta] = filho->subarvore[dir];
    filho->subarvore[dir] = no->subarvore[dir];
    no->subarvore[dir] = filho;
    filho = no->subarvore[dir];
    no->subarvore[dir] = filho->subarvore[oposta];
    filho->subarvore[oposta] = no;
    no = filho;
}

// Para verificar o desempenho da operação de busca (obter total de casos de uma cidade)
int ArvoreAVL::totalCidade(int codCidade, int* comp)
{
    int cont = 0;
    if(raiz != NULL)
        raiz->auxTotalCidade(codCidade, &cont, this->hash, comp);
    return cont;
}

// Retorna a direção oposta
inline Direcao ArvoreAVL::oposta(Direcao dir)
{
    return (dir == direita) ? esquerda : direita;
}

void ArvoreAVL::imprime(){
    if(this->raiz != NULL){
        this->raiz->imprime(0);
    }else{
        cout << "ArvoreAVL vazia!" << endl;
    }
}

void ArvoreAVL::arquivoImpressao(ofstream* arqSaida){
    if(this->raiz != NULL){
        *arqSaida << "* * ArvoreAVL * *" << endl;
        this->raiz->arquivoImpressao(arqSaida, 0);
        *arqSaida << endl;
    }else{
        *arqSaida << "ArvoreAVL vazia!" << endl;
    }
}