/*
 * Arquivo de implementação da classe QuadTree
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004A
 *          Ester Goulart Cruz Rodrigues - 201965028AC
 *          Lucca Oliveira Schrôder - 201765205AC
 *          Rodrigo da Silva Soares - 201765218AB
 * Data de Criação:  08/03/2021
*/

#include "./headers/QuadTree.h"
#include <vector>

//construtor
QuadTree::QuadTree(){
    //cout << "Iniciando QuadTree vazia" << endl;
    raiz = NULL;
}

//destrutor
QuadTree::~QuadTree(){
    delete raiz;
    //cout << "Destruindo QuadTree" << endl;
}

//insere um nó na QuadTree
void QuadTree::inserir(string codEstado, int codCidade, string nomeCidade, float latitude, float longitude, bool capital){
    NoQuadTree* novo = new NoQuadTree(codEstado,codCidade, nomeCidade, latitude, longitude, capital);
    this->inserirAux(novo);
}

void QuadTree::inserirAux(NoQuadTree* P){
    if(this->raiz == NULL){
        this->raiz = P;
    }else{
        NoQuadTree* aux = this->raiz;
        NoQuadTree* pai = NULL;
        string q;   
        while(aux != NULL && !P->equal(aux)){ // falta condição da dimensão, criar metodo
            pai = aux;
            q = aux->compara(P);
            //cout << "Q= " << q << endl;
            aux = aux->quadrante(q);
        }
        //cout << "Saindo While" << aux << endl;
        if( aux == NULL){
            //cout << "Entrando no if" << endl;
            pai->Setquadrante(q, P);
        }
           
    }

}

//RETORNA UM VETOR COM A LISTA DE CIDADE NUM INTERVALO
 vector<int>* QuadTree::pesquisaCodCidade(float latitudeInicial, float longitudeInicial,  float latitudeFinal, float longitudeFinal,int* comp){
    if(this->raiz == NULL){
        throw invalid_argument("QuadTree Vazia!");
    }else{
        vector<int>* vec = new vector<int>();
        raiz->pesquisaCodCidade(latitudeInicial, longitudeInicial, latitudeFinal, longitudeFinal, vec, comp);
        return vec;      
    }

}

//impressao da QuadTree  
void QuadTree::impressao(){

    if(this->raiz == NULL){
        cout << "QuadTree vazia!" << endl;
    }else{
        cout << "IMPRESSAO QUADTREE:" << endl;
        raiz->impressao(0);
    } cout << endl;

}

//impressao da QuadTree no arquivo de saida
void QuadTree::arquivoImpressao(ofstream* arqSaida){

    if(this->raiz == NULL){
        *arqSaida << "QuadTree vazia!" << endl;
    }else{
        *arqSaida << "* * QuadTree * *" << endl;
        raiz->arquivoImpressao(arqSaida,0);
        *arqSaida << endl;
    } cout << endl;

}



