/*
 * Arquivo de implementação da classe nohArvoreAVL
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004A
 *          Ester Goulart Cruz Rodrigues - 201965028AC
 *          Lucca Oliveira Schrôder - 201765205AC
 *          Rodrigo da Silva Soares - 201765218AB
 * Data de Criação:  08/03/2021
*/

#include "headers/nohArvoreAVL.h"

nohArvoreAVL::nohArvoreAVL(int key) 
{
    this->chave = key;
    this->subarvore[esquerda] = NULL;
    this->subarvore[direita] = NULL;
    this->balanceamento = igual; // inicializa as subarvores com a mesma altura
}

nohArvoreAVL::~nohArvoreAVL() // deletar as subarvores
{
    if(this->subarvore[esquerda] != NULL)
    {
        delete this->subarvore[esquerda];
    }
    if(this->subarvore[direita] != NULL)
    {
        delete this->subarvore[direita];
    }
}

void nohArvoreAVL::auxTotalCidade(int codCidade,int* cont, Hash* hash, int* comp)
{
   
    Dado *dadoCidade = hash->buscarHash(chave);

    (*comp)++; // realizou uma comparação
    if(dadoCidade->getCodCidade() == codCidade)
    {        
        *cont += dadoCidade->getCasos();
    }

    // Busca feita em toda árvore, é possível ser otimizado pela propriedade
    if(this->subarvore[esquerda] != NULL)
    {
        this->subarvore[esquerda]->auxTotalCidade(codCidade, cont, hash, comp);
    }
    if(this->subarvore[direita] != NULL)
    {
        this->subarvore[direita]->auxTotalCidade(codCidade, cont, hash, comp);
    }

}

void nohArvoreAVL::imprime(int tab){
    for (int i = 0; i < tab; i++)
        printf("\t");

    cout << this->chave << endl;

    tab++;

    if(this->subarvore[esquerda] != NULL)
    {
        this->subarvore[esquerda]->imprime(tab);
    }
    if(this->subarvore[direita] != NULL)
    {
        this->subarvore[direita]->imprime(tab);
    }

    
}

void nohArvoreAVL::arquivoImpressao(ofstream* arqSaida, int tab){
    for (int i = 0; i < tab; i++)
       *arqSaida << " ";

    tab++;

    *arqSaida << this->chave << endl;


    if(this->subarvore[esquerda] != NULL)
    {
        this->subarvore[esquerda]->arquivoImpressao(arqSaida, tab);
    }
    if(this->subarvore[direita] != NULL)
    {
        this->subarvore[direita]->arquivoImpressao(arqSaida, tab);
    }
}