/*
 * Arquivo de implementação da classe QuadTree
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004A
 *          Ester Goulart Cruz Rodrigues - 201965028AC
 *          Lucca Oliveira Schrôder - 201765205AC
 *          Rodrigo da Silva Soares - 201765218AB
 * Data de Criação:  08/03/2021
*/

#include "./headers/NoQuadTree.h"
#include <vector>

//construtor
NoQuadTree::NoQuadTree(string codEstado, int codCidade, string nomeCidade, float latitude, float longitude, bool capital){
    this->codEstado = codEstado;
    this->codCidade = codCidade;
    this->nomeCidade = nomeCidade;
    this->longitude = longitude;
    this->latitude = latitude;
    this->capital = capital;
    this->NW = this->NE = this->SW = this->SE = NULL;

}

//destrutor
NoQuadTree::~NoQuadTree(){
    if( this->NW != NULL) delete this->NW;
    if( this->NE != NULL) delete this->NE;
    if( this->SW != NULL) delete this->SW;
    if( this->SE != NULL) delete this->SE;
}

// Retorno o quadrante, a saber 1-NE | 2-NW | 3- SW | 4-
string NoQuadTree::compara(NoQuadTree* P){ 
    if( P->getLat() < this->latitude ){
        return P->getLog() < this->longitude ? "SW" : "NW";
    }else{
        return P->getLog() < this->longitude  ? "SE" : "NE";
    }     
}

//retorno o filho dado um qudrante
NoQuadTree* NoQuadTree::quadrante(string sigla){

    if(sigla == "NE"){
        return this->NE;
    }else if(sigla == "NW"){
        return this->NW;
    }else if(sigla == "SW"){
        return this->SW;
    }else{
        return this->SE;
    }

}

//seta o valor pro novo quadrante
void NoQuadTree::Setquadrante(string sigla, NoQuadTree* novo){

    if(sigla == "NE"){
        this->NE = novo; return;
    }else if(sigla == "NW"){
        this->NW = novo; return;
    }else if(sigla == "SW"){
        this->SW = novo; return;
    }else{
        this->SE = novo;
    }

}

float NoQuadTree::getLat(){
    return this->latitude;
}

float NoQuadTree::getLog(){
    return this->longitude;
} 

//verifica se um nph eh igual a outro
bool NoQuadTree::equal(NoQuadTree* P){
    return this->latitude == P->getLat() && this->longitude == P->getLog() ;
}

//imprime o noh da arvore
void NoQuadTree::impressao(int prof){

    for (int i = 0; i < prof; i++)
        printf("\t");

    prof++;

//    cout << this->nomeCidade << " Lat: " << this->latitude << " Log: " << this->longitude << endl;
    cout << this->nomeCidade << endl;
    if( this->NE != NULL){
        cout << "NE: ";
        this->NE->impressao(prof); 
    }

    if( this->NW != NULL){
        cout << "NW: ";
        this->NW->impressao(prof);   
    } 

    if( this->SW != NULL){
        cout << "SW: ";
      this->SW->impressao(prof);  
    } 

    if( this->SE != NULL){
        cout << "SE: ";
        this->SE->impressao(prof);
    } 

    
}

//pesquisa grupo de cidade num intervalo
void NoQuadTree::pesquisaCodCidade(float latitudeInicial, float longitudeInicial,  float latitudeFinal, float longitudeFinal, vector<int>* vec, int* comp){
    if(this->latitude >= latitudeInicial && this->latitude <= latitudeFinal 
            && this->longitude >= longitudeInicial && this->longitude <= longitudeFinal){
                (*comp) += 4; // foram realizadas 4 comparações
        vec->push_back(codCidade);
    }

    if(this->NE != NULL) NE->pesquisaCodCidade(latitudeInicial, longitudeInicial, latitudeFinal, longitudeFinal, vec, comp);
    if(this->NW != NULL) NW->pesquisaCodCidade(latitudeInicial, longitudeInicial, latitudeFinal, longitudeFinal, vec, comp);
    if(this->SW != NULL) SW->pesquisaCodCidade(latitudeInicial, longitudeInicial, latitudeFinal, longitudeFinal, vec, comp);
    if(this->SE != NULL) SE->pesquisaCodCidade(latitudeInicial, longitudeInicial, latitudeFinal, longitudeFinal, vec, comp);
}

//imprime do no da árvore no arquivo de saida
void NoQuadTree::arquivoImpressao(ofstream* arqSaida, int prof){

    for (int i = 0; i < prof; i++)
        *arqSaida << "\t";

    prof++;

//    cout << this->nomeCidade << " Lat: " << this->latitude << " Log: " << this->longitude << endl;
    *arqSaida << this->nomeCidade << endl;
    if( this->NE != NULL){
        *arqSaida << "NE: ";
        this->NE->arquivoImpressao(arqSaida, prof); 
    }

    if( this->NW != NULL){
        *arqSaida << "NW: ";
        this->NW->arquivoImpressao(arqSaida, prof); 
    } 

    if( this->SW != NULL){
        *arqSaida << "SW: ";
      this->SW->arquivoImpressao(arqSaida, prof); 
    } 

    if( this->SE != NULL){
        *arqSaida << "SE: ";
        this->SE->arquivoImpressao(arqSaida, prof); 
    } 

    
}