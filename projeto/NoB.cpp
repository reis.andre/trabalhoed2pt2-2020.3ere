/*
 * Arquivo de implementação da classe NoB
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004A
 *          Ester Goulart Cruz Rodrigues - 201965028AC
 *          Lucca Oliveira Schrôder - 201765205AC
 *          Rodrigo da Silva Soares - 201765218AB
 * Data de Criação:  08/03/2021
*/

#include "headers/NoB.h"

NoB::NoB(int ordem, bool folha)
{
    this->ordem = ordem; // A ORDEM SERIA O NUMERO MÍNIMO DE ELEMENTOS NA ARVORE
    this->folha = folha;

    this->infos = new int[2 * ordem - 1];
    this->filhos = new NoB *[2 * ordem];

    this->totalElementos = 0;
}

NoB::~NoB() // PENDENTE
{
    //DESTRUTOR CONSTRUIR ELE
}

void NoB::imprime(int tab) // OK
{
    for (int i = 0; i < tab; i++)
        printf("\t");

    int i;
    for ( i = 0; i < totalElementos; i++)
    {
        if (!folha)
        {
            tab++;
            filhos[i]->imprime(tab);
        }
        cout << infos[i] << " ";
    }
    cout << endl;

    if (!folha){ // IR PRO ULTIMO FILHO?
        filhos[i]->imprime(tab);
    }
}

NoB *NoB::busca(int info) // !!! alterar ordenação par (código da cidade, data) e não pelo valor no nó
{
    int i = 0;

    while (i < totalElementos && info > infos[i]) // al
        i++;

    if (infos[i] == info)
        return this;

    if (folha == true)
        return NULL;

    return filhos[i]->busca(info);
}

int NoB::getAntecessor(int indice) // Retorna o nó anterior ao nó correspondente ao índice
{
    NoB *antecessor = filhos[indice];
    while (!antecessor->folha)
        antecessor = antecessor->filhos[antecessor->totalElementos];

    return antecessor->infos[antecessor->totalElementos - 1];
}

int NoB::getSucessor(int indice) // Retorna o próximo nó do nó correspondente ao índice
{
    NoB *sucessor = filhos[indice + 1];
    while (!sucessor->folha)
        sucessor = sucessor->filhos[0];

    return sucessor->infos[0];
}

void NoB::divideFilhos(int i, NoB *n)
{
    NoB *z = new NoB(n->ordem, n->folha);
    z->totalElementos = ordem - 1;

    for (int j = 0; j < ordem - 1; j++)
        z->infos[j] = n->infos[j + ordem];

    if (n->folha == false)
    {
        for (int j = 0; j < ordem; j++)
            z->filhos[j] = n->filhos[j + ordem];
    }

    n->totalElementos = ordem - 1;
    for (int j = totalElementos; j >= i + 1; j--)
        filhos[j + 1] = filhos[j];

    filhos[i + 1] = z;

    for (int j = totalElementos - 1; j >= i; j--)
        infos[j + 1] = infos[j];

    infos[i] = n->infos[ordem - 1];
    totalElementos = totalElementos + 1;
}

void NoB::insereAux(int info)
{

    int i = totalElementos - 1;

    if (folha == true) 
    {
        while (i >= 0 && infos[i] > info) // BUSCA PELO PAR ATRAVES DO HASH
        {
            infos[i + 1] = infos[i];
            i--;
        }

        infos[i + 1] = info;
        totalElementos = totalElementos + 1;
    }
    else
    {
        while (i >= 0 && infos[i] > info) // BUSCA PELO PAR ATRAVES DO HASH
            i--;

        if (filhos[i + 1]->totalElementos == 2 * ordem - 1) // SE ESTIVER LOTADO DIVIDE FILHO, SHOW
        {
            divideFilhos(i + 1, filhos[i + 1]);

            if (infos[i + 1] < info)
                i++;
        }
        filhos[i + 1]->insereAux(info); // FAZ A NOVA INSERÇÃO
    }
}

void NoB::auxTotalCidade(int codCidade,int* cont, Hash* hash, int* comp)
{
   
    Dado* dadoCidade;

    int i;
    for ( i = 0; i < totalElementos; i++)
    {
        dadoCidade = hash->buscarHash(infos[i]);
        
        (*comp)++; // realizou uma comparação
        if(dadoCidade->getCodCidade() == codCidade)
        {
            *cont += dadoCidade->getCasos();
        }

        if (!folha) // chama a recursao
        {            
            filhos[i]->auxTotalCidade(codCidade,cont,hash,comp);
        }
    }

    if (!folha){ // RECURSAO PRO ULTIMO
        filhos[i]->auxTotalCidade(codCidade,cont,hash, comp);
    }


}

void NoB::arquivoImpressao(ofstream* arqSaida, int tab){

    for (int i = 0; i < tab; i++)
       *arqSaida << "\t";

    int i;
    for ( i = 0; i < totalElementos; i++)
    {
        if (!folha)
        {
            tab++;
            filhos[i]->arquivoImpressao(arqSaida, tab);
        }
        *arqSaida << infos[i] << " ";
    }
    *arqSaida << endl;

    if (!folha){ // IR PRO ULTIMO FILHO?
        filhos[i]->arquivoImpressao(arqSaida, tab);
    }

}

